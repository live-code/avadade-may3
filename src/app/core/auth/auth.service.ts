import { Injectable } from '@angular/core';
import { Auth } from '../../model/auth';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  data: Auth | null = null;

  constructor(private http: HttpClient, private router: Router) {
    const dataFromLocalStorage: string | null = localStorage.getItem('auth');
    if(dataFromLocalStorage) {
      this.data = JSON.parse(dataFromLocalStorage) as Auth
    }
  }

  login(username: string, password: string) {
    const params = new HttpParams()
      .set('user', username)
      .set('pass', password)

    this.http.get<Auth>(`http://localhost:3000/login`, { params }) // object concise syntax
      .subscribe(res => {
        this.data = res;
        localStorage.setItem('auth', JSON.stringify(res))
        this.router.navigateByUrl('home')
      })

  }

  logout() {
    this.data = null;
    localStorage.removeItem('auth')
    this.router.navigateByUrl('login')
  }

  get isLogged(): boolean {
    return !!this.data;
    // return this.data ? true : false
  }

  get token(): string | null {
    return this.data ? this.data.token : null
  }
}
