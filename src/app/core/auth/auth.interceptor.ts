import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { catchError, Observable, of, throwError } from 'rxjs';
import { AuthService } from './auth.service';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(private authService: AuthService, private router: Router) {
  }
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let clonedReq = req;

    if (this.authService.token && req.url.includes('localhost')) {
      clonedReq = req.clone({
        setHeaders: {
          token: this.authService.token,
        }
      })
    }
    return next.handle(clonedReq)
      .pipe(
        catchError(e => {
          switch (e.status) {
            case 404:
              // toast.show('errore lato server')
              // this.router.navigateByUrl('login')
              break;
            case 401:
              this.router.navigateByUrl('login')
              break;
          }
          return throwError(e);
        })
      )
  }

}


// --o----o---o-o-----o---o--xo o o o o ----------->
