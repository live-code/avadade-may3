import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth/auth.service';

@Component({
  selector: 'ava-navbar',
  template: `
    <button routerLink="login" routerLinkActive="active">login</button>
    <button routerLink="home" routerLinkActive="active">home</button>
    <button routerLink="catalog" routerLinkActive="active" *ngIf="authService.isLogged">catalog</button>
    <button routerLink="contacts" routerLinkActive="active">contacts</button>
    <button (click)="authService.logout()" 
            *ngIf="authService.isLogged">Logout</button>
    {{authService.data?.displayName}} - {{authService.data?.token}} -
  `,
  styles: [`
    .active { background-color: orange }
  `]
})
export class NavbarComponent implements OnInit {

  constructor(public authService: AuthService) { }

  ngOnInit(): void {
  }

}
