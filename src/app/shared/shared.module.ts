import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardComponent } from './components/card.component';
import { TabbarComponent } from './components/tabbar.component';
import { CardModule } from './components/card.module';



@NgModule({
  declarations: [
    TabbarComponent
  ],
  imports: [
    CommonModule,
    CardModule,
  ],
  exports: [
    TabbarComponent,
    CardModule
  ]
})
export class SharedModule { }
