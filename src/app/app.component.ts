import { Component } from '@angular/core';

@Component({
  selector: 'ava-root',
  template: `
    <ava-navbar></ava-navbar>
    <router-outlet></router-outlet>
  `,
})
export class AppComponent {
}
