import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LoginComponent } from './login.component';
import { LostPassComponent } from './components/lost-pass.component';
import { RegistrationComponent } from './components/registration.component';
import { SignInComponent } from './components/sign-in.component';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    LoginComponent,
      LostPassComponent,
      RegistrationComponent,
      SignInComponent
  ],
  imports: [
    CommonModule,
    FormsModule,

    RouterModule.forChild(
      [
        {
          path: '',
          component: LoginComponent,
          children: [
            { path: 'registration', component: RegistrationComponent},
            { path: 'lostpass', component: LostPassComponent},
            { path: 'signin', component: SignInComponent},
            { path: '', redirectTo: 'signin' }
          ]
        },
      ]
    )
  ]
})
export class LoginModule { }
