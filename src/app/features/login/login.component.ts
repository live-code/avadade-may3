import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ava-login',
  template: `
    <h1>Welcome to Avanade</h1>
    <router-outlet></router-outlet>
    <hr>
    <button routerLink="signin" routerLinkActive="active">signin</button>
    <button routerLink="registration" routerLinkActive="active">registration</button>
    <button routerLink="lostpass"  routerLinkActive="active">lostpass</button>
  `,
  styles: [`
    .active {background-color: purple; color: white}
  `]
})
export class LoginComponent {
  page = 'sign-in'

}
