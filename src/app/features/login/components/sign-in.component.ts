import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Auth } from '../../../model/auth';
import { AuthService } from '../../../core/auth/auth.service';

@Component({
  selector: 'ava-sign-in',
  template: `
    <h2>sign-in !</h2>
    {{authService.data?.token}}
    <form #f="ngForm" (submit)="login(f.value)">
      <input type="text" ngModel name="user" >
      <input type="text" ngModel name="pass" >
      <button type="submit" [disabled]="f.invalid">LOGIN</button>
    </form>
  `,
})
export class SignInComponent  {

  constructor(public authService: AuthService) {}

  login(formData: any) {
    this.authService.login(
      formData.user,
      formData.pass
    )

  }
}
