import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { SharedModule } from '../../shared/shared.module';
import { PhotoEditorComponent } from './components/photo-editor/photo-editor.component';
import { PhotoPanelComponent } from './components/photo-editor/photo-panel.component';
import { PhotoCharactersComponent } from './components/photo-editor/photo-characters.component';
import { PhotoTimelineComponent } from './components/photo-editor/photo-timeline.component';
import { PhotoEditorModule } from './components/photo-editor/photo-editor.module';


@NgModule({
  declarations: [
    HomeComponent,
  ],
  imports: [
    PhotoEditorModule,
    CommonModule,
    HomeRoutingModule,
    SharedModule
  ]
})
export class HomeModule { }
