import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ava-home',
  template: `
    <p>
      home works!
    </p>
    <ava-card></ava-card>
    <ava-photo-editor></ava-photo-editor>
  `,
  styles: [
  ]
})
export class HomeComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
