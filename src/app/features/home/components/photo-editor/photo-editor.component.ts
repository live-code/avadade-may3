import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ava-photo-editor',
  template: `
    <h1>photo editor component</h1>
    <ava-photo-characters></ava-photo-characters>
    <ava-photo-timeline></ava-photo-timeline>
  `,
  styles: [
  ]
})
export class PhotoEditorComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
