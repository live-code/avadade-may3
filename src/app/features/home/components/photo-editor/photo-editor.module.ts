import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PhotoEditorComponent } from './photo-editor.component';
import { PhotoPanelComponent } from './photo-panel.component';
import { PhotoCharactersComponent } from './photo-characters.component';
import { PhotoTimelineComponent } from './photo-timeline.component';



@NgModule({
  declarations: [
    PhotoEditorComponent,
      PhotoPanelComponent,
      PhotoCharactersComponent,
      PhotoTimelineComponent
  ],
  exports: [
    PhotoEditorComponent
  ],
  imports: [
    CommonModule
  ]
})
export class PhotoEditorModule { }
