import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AuthService } from '../../core/auth/auth.service';
import { catchError, interval, map, of } from 'rxjs';

@Component({
  selector: 'ava-catalog',
  template: `
    <p>
      catalog works!
    </p>
    
    <div *ngIf="error">AHIA|!!!!!!</div>
    <ava-card></ava-card>
  `,
  styles: [
  ]
})
export class CatalogComponent implements OnInit {
 error: boolean = false;
  constructor(private http: HttpClient, private authSrv: AuthService) {


      http.get<any[]>('http://localhost:3000/catalog2')
        .subscribe(
          res => {
            console.log(res)
          },
          err => {
            this.error = true;
          }
        )
  }

  ngOnInit(): void {
  }

}
