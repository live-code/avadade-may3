import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CatalogRoutingModule } from './catalog-routing.module';
import { CatalogComponent } from './catalog.component';
import { SharedModule } from '../../shared/shared.module';
import { CardComponent } from '../../shared/components/card.component';
import { CardModule } from '../../shared/components/card.module';


@NgModule({
  declarations: [
    CatalogComponent,
  ],
  imports: [
    CommonModule,
    CardModule,
    CatalogRoutingModule,
  ]
})
export class CatalogModule { }
